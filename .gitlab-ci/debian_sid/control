Source: xrdesktop
Priority: optional
Maintainer: Andrew Lee (李健秋) <ajqlee@debian.org>
Uploaders: Héctor Orón Martínez <zumbi@debian.org>
Build-Depends: debhelper (>= 11),
               dh-python,
               glslang-tools,
               libgxr-dev (>= 0.16),
               libg3k-dev (>= 0.16),
               meson,
               python3-dev,
               python-gi-dev,
Standards-Version: 4.3.0
Section: libs
Homepage: https://gitlab.freedesktop.org/xrdesktop/xrdesktop/
Vcs-Browser: https://salsa.debian.org/xrdesktop-team/xrdesktop
Vcs-Git: https://salsa.debian.org/xrdesktop-team/xrdesktop.git

Package: xrdesktop
Section: utils
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends},
         libxrdesktop-0.16-0 (>= ${source:Version})
Recommends: kwin-effect-xrdesktop | gnome-shell-xrdesktop
Description: XR desktop interaction library -- settings application
 library for XR interaction with classical desktop compositors.
 .
 This package includes the settings application.

Package: libxrdesktop-0.16-0
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}, libg3k-0.16-0
Replaces: libxrdesktop-0.13-0 (<< 0.16), libxrdesktop-0.14-0 (<< 0.16)
Breaks: libxrdesktop-0.13-0 (<< 0.16), libxrdesktop-0.14-0 (<< 0.16)
Description: XR desktop interaction library
 library for XR interaction with classical desktop compositors.
 .
 This package includes the loader library.

Package: libxrdesktop-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         libgxr-dev,
         libg3k-dev,
         libxrdesktop-0.16-0 (= ${binary:Version}),
Description: XR desktop interaction library -- development files
 library for XR interaction with classical desktop compositors.
 .
 This package includes files needed for development.
