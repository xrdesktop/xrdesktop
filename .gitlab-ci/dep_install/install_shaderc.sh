#!/bin/bash

# skip this on arch
if [ -f "/etc/arch-release" ]; then
  exit
fi

mkdir -p deps
cd deps

git clone --branch $1 https://github.com/google/shaderc.git
cd shaderc

./utils/git-sync-deps

cmake . \
  -G Ninja \
  -B build \
  -DCMAKE_INSTALL_PREFIX=/usr \
  -DCMAKE_BUILD_TYPE=Release \
  -DSHADERC_SKIP_TESTS=ON
ninja -C build install
cd ../..
