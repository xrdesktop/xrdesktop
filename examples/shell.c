/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include <glib-unix.h>
#include <glib.h>

#include <g3k.h>
#include <xrd.h>

#define GRID_WIDTH 4
#define GRID_HEIGHT 4

typedef struct Example
{
  GMainLoop *loop;
  XrdShell  *shell;

  G3kButton *restart_button;
  gboolean   restart;
  G3kButton *quit_button;

  G3kButton *open_window_button;
  G3kButton *close_window_button;
  XrdWindow *extra_window;

  GulkanTexture *cursor_texture;
  guint64        click_source;
  guint64        move_source;
  gboolean       shutdown;

  GdkPixbuf *window_pixbuf;
  GdkPixbuf *bw_window_pixbuf;
  GdkPixbuf *child_window_pixbuf;

  /* A real window manager will have a function like
   * _desktop_window_process_frame when updating a window's contents.
   * In this example, this is simulated by calling the window
   * update function for each desktop window in a imer. */
  GSList *desktop_window_list;
  gint64  desktop_window_manager_update_loop;

  /*
   * Access to a window's GulkanTexture must be synchronized.
   * E.g. a destroying a window while a texture is uploaded is UB.
   *
   * For this example, access to desktop_window_list will also be synchronized
   * with this mutex, because
   */
  GMutex window_mutex;

  GThread      *upload_thread;
  volatile gint shutdown_threads;

  GRand   *rand;
  gboolean saturating;

  gboolean rendering;
  gboolean framecycle;
} Example;

/* Placeholder struct for a real desktop window manager's window struct.
 * Examples are KWin::EffectWindow (kwin plugin) and MetaWindow (gnome).
 *
 * When a per window state should be saved (for example an OpenGL texture for
 * external memory, create a Wrapper struct for a DesktopWindow and save
 * that in a XrdWindow's `native` instead of the DesktopWindow struct. Ex.
 * struct WindowWrapper {
 *   KWin::EffectWindow *desktop_window;
 *   GLuint *opengl_texture;
 * }
 */
typedef struct DesktopWindow
{
  GdkPixbuf *pixbuf;
  gchar     *title;
  gboolean   is_desaturated;

  XrdWindowRect rect;
} DesktopWindow;

/* In a real desktop window manager, the wm creates window structs, not us. */
static DesktopWindow *
_create_desktop_window (Example *self, gchar *title, GdkPixbuf *pixbuf)
{
  g_mutex_lock (&self->window_mutex);

  DesktopWindow *desktop_window = g_malloc (sizeof (DesktopWindow));
  desktop_window->is_desaturated = false;
  desktop_window->pixbuf = pixbuf;
  desktop_window->title = g_strdup (title);
  desktop_window->rect = (XrdWindowRect) {
    .bl = {
      .x = 0,
      .y = 0,
    },
    .tr = {
      .x = (uint32_t) gdk_pixbuf_get_width (pixbuf),
      .y = (uint32_t) gdk_pixbuf_get_height (pixbuf),
    }
  };

  self->desktop_window_list = g_slist_append (self->desktop_window_list,
                                              desktop_window);
  g_mutex_unlock (&self->window_mutex);

  return desktop_window;
}

static gboolean
_desktop_window_process_frame (Example *self, DesktopWindow *desktop_window);

static gboolean
_desktop_window_manager_update_textures (gpointer _self)
{
  Example *self = _self;
  for (GSList *l = self->desktop_window_list; l; l = l->next)
    {
      DesktopWindow *desktop_window = l->data;
      _desktop_window_process_frame (self, desktop_window);
    }
  return TRUE;
}

static gboolean
_sigint_cb (gpointer _self)
{
  Example *self = (Example *) _self;
  g_main_loop_quit (self->loop);
  return TRUE;
}

static GdkPixbuf *
load_gdk_pixbuf (const gchar *name)
{
  GError    *error = NULL;
  GdkPixbuf *pixbuf_rgb = gdk_pixbuf_new_from_resource (name, &error);

  if (error != NULL)
    {
      g_printerr ("Unable to read file: %s\n", error->message);
      g_error_free (error);
      return NULL;
    }

  GdkPixbuf *pixbuf = gdk_pixbuf_add_alpha (pixbuf_rgb, FALSE, 0, 0, 0);
  g_object_unref (pixbuf_rgb);

  return pixbuf;
}

static GulkanTexture *
_desktop_window_to_texture (Example *self, DesktopWindow *desktop_window)
{
  G3kContext    *g3k = xrd_shell_get_g3k (self->shell);
  GulkanContext *gulkan = g3k_context_get_gulkan (g3k);
  VkImageLayout  layout = g3k_context_get_upload_layout (g3k);

  GulkanTexture *tex = gulkan_texture_new_from_pixbuf (gulkan,
                                                       desktop_window->pixbuf,
                                                       VK_FORMAT_R8G8B8A8_SRGB,
                                                       layout, FALSE);
  return tex;
}

static gboolean
_desktop_window_process_frame (Example *self, DesktopWindow *desktop_window)
{
  XrdWindow *xrd_window = xrd_shell_lookup_window (self->shell, desktop_window);
  if (!xrd_window)
    {
      g_print ("Error processing frame, window is NULL\n");
      return TRUE;
    }

  guint window_texture_width = (guint)
    gdk_pixbuf_get_width (desktop_window->pixbuf);
  guint window_texture_height = (guint)
    gdk_pixbuf_get_height (desktop_window->pixbuf);

  GulkanTexture *cached_texture
    = g3k_plane_get_texture (G3K_PLANE (xrd_window));

  /* we need to create a new texture if we have not submitted a texture yet
   * or if the window size changed. */
  VkExtent2D extent;
  if (cached_texture)
    extent = gulkan_texture_get_extent (cached_texture);

  if (cached_texture == NULL || window_texture_width != extent.width
      || window_texture_height != extent.height)
    {
      g_debug ("Allocating new texture for %s", desktop_window->title);
      cached_texture = _desktop_window_to_texture (self, desktop_window);

      /* Failing to creating a texture for a window should never happen. */
      if (!GULKAN_IS_TEXTURE (cached_texture))
        {
          g_print ("Error creating texture for window!\n");
          return FALSE;
        }

      g_debug ("Set texture for %s", desktop_window->title);
      g3k_plane_set_texture (G3K_PLANE (xrd_window), cached_texture);
      xrd_window_set_rect (xrd_window, &desktop_window->rect);
    }

  return TRUE;
}

static XrdWindow *
_add_window (Example       *self,
             DesktopWindow *desktop_window,
             G3kObject     *parent,
             float          width_meters,
             gboolean       draggable)

{
  /* a real window manager needs to find these from real desktop windows. */
  gchar *title = desktop_window->title;

  VkExtent2D extent = {
    desktop_window->rect.tr.x - desktop_window->rect.bl.x,
    desktop_window->rect.tr.y - desktop_window->rect.bl.y,
  };

  float ppm = (float) extent.width / width_meters;

  graphene_size_t size_meters = {
    width_meters,
    (float) extent.height / ppm,
  };

  XrdWindow *window = xrd_window_new (xrd_shell_get_g3k (self->shell), title,
                                      desktop_window, extent, &size_meters);

  xrd_shell_add_window (self->shell, window, parent, draggable, desktop_window);

  return window;
}

static gboolean
_init_shell (Example *self, XrdShell *shell);
static void
_cleanup_shell (Example *self);
static gboolean
_init_threads (Example *self);

static void
_button_quit_press_cb (XrdWindow     *window,
                       GxrController *controller,
                       gpointer       _self)
{
  (void) controller;
  (void) window;
  Example *self = _self;

  self->restart = FALSE;
  g_main_loop_quit (self->loop);
}

static void
_button_restart_press_cb (XrdWindow     *window,
                          GxrController *controller,
                          gpointer       _self)
{
  (void) controller;
  (void) window;
  Example *self = _self;

  self->restart = TRUE;
  g_main_loop_quit (self->loop);
}

static void
_button_open_window_press_cb (XrdWindow     *window,
                              GxrController *controller,
                              gpointer       _self)
{
  (void) controller;
  (void) window;

  Example          *self = _self;
  G3kContext       *g3k = xrd_shell_get_g3k (self->shell);
  G3kObjectManager *manager = g3k_context_get_manager (g3k);

  if (self->extra_window)
    {
      g_print ("Already opened extra window, skipping\n");
      return;
    }

  float window_width_meter = 1.0f;

  gchar window_title[16];
  sprintf (window_title, "Extra Bird");
  DesktopWindow *desktop_window = _create_desktop_window (self, window_title,
                                                          self->window_pixbuf);

  self->extra_window = _add_window (self, desktop_window, NULL,
                                    window_width_meter, TRUE);

  graphene_point3d_t point = {.x = 1.5, .y = 1.5, .z = -2.9f};
  graphene_matrix_t  transform;
  graphene_matrix_init_translate (&transform, &point);
  g3k_object_set_matrix (G3K_OBJECT (self->extra_window), &transform);

  g3k_object_manager_save_reset_transformation (manager,
                                                G3K_OBJECT (self
                                                              ->extra_window));

  _desktop_window_manager_update_textures (self);
}

static void
_button_close_window_press_cb (XrdWindow     *window,
                               GxrController *controller,
                               gpointer       _self)
{
  (void) controller;
  (void) window;
  Example *self = _self;

  if (!self->extra_window)
    {
      g_print ("No window to close, skipping\n");
      return;
    }

  g_mutex_lock (&self->window_mutex);

  DesktopWindow *desktop_window = NULL;
  g_object_get (self->extra_window, "native", &desktop_window, NULL);
  if (!desktop_window)
    {
      g_print ("Closed window without native!\n");
      g_mutex_unlock (&self->window_mutex);
      return;
    }

  self->desktop_window_list = g_slist_remove (self->desktop_window_list,
                                              desktop_window);

  g_object_set (self->extra_window, "native", NULL, NULL);
  xrd_shell_remove_window (self->shell, self->extra_window);

  xrd_window_close (self->extra_window);
  g_clear_object (&self->extra_window);

  g_free (desktop_window);
  g_mutex_unlock (&self->window_mutex);
}

static void
_init_child_window (Example *self, XrdWindow *window)
{
  DesktopWindow *desktop_window
    = _create_desktop_window (self, "A child", self->child_window_pixbuf);

  XrdWindow *child = _add_window (self, desktop_window, G3K_OBJECT (window),
                                  0.25f, FALSE);

  float ppm = g3k_plane_get_global_ppm (G3K_PLANE (window));

  graphene_point3d_t offset_point = {
    .x = g3k_to_meters (-150, ppm),
    .y = g3k_to_meters (600, ppm),
    .z = 0.1f,
  };
  G3kPose offset = g3k_pose_new (&offset_point, NULL);

  g3k_object_set_local_pose (G3K_OBJECT (child), &offset);

  xrd_window_add_child (window, child);
}

static gboolean
_init_cursor (Example *self)
{
  G3kContext   *g3k = xrd_shell_get_g3k (self->shell);
  VkImageLayout layout = g3k_context_get_upload_layout (g3k);

  GulkanContext *gulkan = g3k_context_get_gulkan (g3k);
  self->cursor_texture = gulkan_texture_new_from_resource (gulkan,
                                                           "/res/cursor.png",
                                                           layout, TRUE);

  G3kCursor *cursor = xrd_shell_get_desktop_cursor (self->shell);
  g3k_cursor_set_hotspot (cursor, 3, 3);
  g3k_plane_set_texture (G3K_PLANE (cursor), self->cursor_texture);

  return TRUE;
}

static void
_init_buttons (Example *self)
{
  graphene_point3d_t button_pos = {.x = -2.5f, .y = 0.3f, .z = -2.5f};
  graphene_size_t    size_meters = {.6f, .6f};

  G3kContext *g3k = xrd_shell_get_g3k (self->shell);

  gchar *quit_str[] = {"Quit"};
  self->quit_button = g3k_button_new (g3k, &size_meters);
  if (!self->quit_button)
    return;
  g3k_button_set_text (self->quit_button, 1, quit_str);
  xrd_shell_add_button (self->shell, self->quit_button, &button_pos,
                        (GCallback) _button_quit_press_cb, self, NULL);

  graphene_size_t button_size
    = g3k_plane_get_global_size_meters (G3K_PLANE (self->quit_button));
  button_pos.x += button_size.width;

  gchar *restart_str[] = {"Restart"};
  self->restart_button = g3k_button_new (g3k, &size_meters);
  if (!self->restart_button)
    return;

  g3k_button_set_text (self->restart_button, 1, restart_str);

  xrd_shell_add_button (self->shell, self->restart_button, &button_pos,
                        (GCallback) _button_restart_press_cb, self, NULL);

  button_pos.x += button_size.width;

  gchar *open_window_str[] = {"Open", "a window"};
  self->open_window_button = g3k_button_new (g3k, &size_meters);
  if (!self->open_window_button)
    return;

  g3k_button_set_text (self->open_window_button, G_N_ELEMENTS (open_window_str),
                       open_window_str);

  xrd_shell_add_button (self->shell, self->open_window_button, &button_pos,
                        (GCallback) _button_open_window_press_cb, self, NULL);

  button_pos.x += button_size.width;

  gchar *close_window_str[] = {"Close", "a window"};
  self->close_window_button = g3k_button_new (g3k, &size_meters);
  if (!self->close_window_button)
    return;

  g3k_button_set_text (self->close_window_button,
                       G_N_ELEMENTS (close_window_str), close_window_str);

  xrd_shell_add_button (self->shell, self->close_window_button, &button_pos,
                        (GCallback) _button_close_window_press_cb, self, NULL);
}

static gboolean
_init_windows (Example *self)
{
  float window_x = -1.f;
  float window_y = 0;

  float window_width_meter = 0.5f;

  G3kContext       *g3k = xrd_shell_get_g3k (self->shell);
  G3kObjectManager *manager = g3k_context_get_manager (g3k);

  /* A window manager iterates over current windows and decides which of those
   * should be mirrored. This example creates a grid with placeholders. */
  for (int col = 0; col < GRID_WIDTH; col++)
    {
      for (int row = 0; row < GRID_HEIGHT; row++)
        {
          gchar window_title[16];
          sprintf (window_title, "Bird [%d,%d]", col, row);
          DesktopWindow *desktop_window
            = _create_desktop_window (self, window_title, self->window_pixbuf);

          XrdWindow *window = _add_window (self, desktop_window, NULL,
                                           window_width_meter, TRUE);

          graphene_size_t window_size
            = g3k_plane_get_global_size_meters (G3K_PLANE (window));

          graphene_point3d_t point = {.x = window_x, .y = window_y, .z = -3};
          graphene_matrix_t  transform;
          graphene_matrix_init_translate (&transform, &point);
          g3k_object_set_matrix (G3K_OBJECT (window), &transform);

          g3k_object_manager_save_reset_transformation (manager,
                                                        G3K_OBJECT (window));

          XrdWindowRect *r = xrd_window_get_rect (window);

          g_debug ("Created %dx%d window with %fm width -> %fm height at "
                   "%f,%f,%f",
                   r->tr.x - r->bl.x, r->tr.y - r->bl.y, window_width_meter,
                   window_size.height, point.x, point.y, point.z);

          if ((col + row) % 2 == 0)
            xrd_window_set_flip_y (window, TRUE);

          if (col == 0 && row == 0)
            _init_child_window (self, window);

          window_y += window_size.height;
        }
      window_x += window_width_meter;
      window_y = 0;
    }

  // make 4 windows that each show a quarter of the texture
  window_width_meter = 0.5f;
  for (int col = 0; col < 2; col++)
    {
      uint32_t half_width = (uint32_t)
                              gdk_pixbuf_get_width (self->window_pixbuf)
                            / 2;
      uint32_t half_height = (uint32_t)
                               gdk_pixbuf_get_height (self->window_pixbuf)
                             / 2;

      for (int row = 0; row < 2; row++)
        {
          gchar        *title = NULL;
          XrdWindowRect rect = {0};
          if (col == 0 && row == 0)
            {
              title = "Bird bottom left";
              rect = (XrdWindowRect) {
                .bl = {
                  .x = 0,
                  .y = 0,
                },
                .tr = {
                  .x = half_width,
                  .y = half_height,
                }
              };
            }
          else if (col == 0 && row == 1)
            {
              title = "Bird top left";
              rect = (XrdWindowRect) {
                .bl = {
                  .x = 0,
                  .y = half_height,
                },
                .tr = {
                  .x = half_width,
                  .y = half_height * 2,
                }
              };
            }
          else if (col == 1 && row == 0)
            {
              title = "Bird bottom right";
              rect = (XrdWindowRect) {
                .bl = {
                  .x = half_width,
                  .y = 0,
                },
                .tr = {
                  .x = half_width * 2,
                  .y = half_height,
                }
              };
            }
          else if (col == 1 && row == 1)
            {
              title = "Bird top right";
              rect = (XrdWindowRect) {
                .bl = {
                  .x = half_width,
                  .y = half_height,
                },
                .tr = {
                  .x = half_width * 2,
                  .y = half_height * 2,
                }
              };
            }
          DesktopWindow *desktop_window
            = _create_desktop_window (self, title, self->window_pixbuf);
          desktop_window->rect = rect;
          XrdWindow      *window = _add_window (self, desktop_window, NULL,
                                                window_width_meter, TRUE);
          graphene_size_t window_size
            = g3k_plane_get_global_size_meters (G3K_PLANE (window));

          graphene_point3d_t point = {.x = window_x, .y = window_y, .z = -3};
          graphene_matrix_t  transform;
          graphene_matrix_init_translate (&transform, &point);
          g3k_object_set_matrix (G3K_OBJECT (window), &transform);

          g3k_object_manager_save_reset_transformation (manager,
                                                        G3K_OBJECT (window));

          window_y += window_size.height;
        }

      window_x += window_width_meter;
      window_y = 0;
    }

  return TRUE;
}

static void
_cleanup_shell (Example *self)
{
  g_signal_handler_disconnect (self->shell, self->click_source);
  g_signal_handler_disconnect (self->shell, self->move_source);
  self->click_source = 0;
  self->move_source = 0;

  g_atomic_int_set (&self->shutdown_threads, 1);
  if (self->upload_thread)
    {
      g_thread_join (self->upload_thread);
      self->upload_thread = NULL;
    }
  g_atomic_int_set (&self->shutdown_threads, 0);
}

static void
_cleanup (Example *self)
{
  self->shutdown = TRUE;

  _cleanup_shell (self);

  G3kContext       *g3k = xrd_shell_get_g3k (self->shell);
  G3kObjectManager *manager = g3k_context_get_manager (g3k);
  GSList           *objects = g3k_object_manager_get_objects (manager);
  for (GSList *l = objects; l; l = l->next)
    {
      G3kObject *object = l->data;

      DesktopWindow *desktop_window = NULL;
      g_object_get (object, "native", &desktop_window, NULL);
      g_free (desktop_window->title);
      /* desktop_window->pixbuf is not owned by desktop_window */

      xrd_window_close (XRD_WINDOW (object));
    }
  g_slist_free_full (self->desktop_window_list, g_free);

  g_object_unref (self->bw_window_pixbuf);
  g_object_unref (self->window_pixbuf);
  g_object_unref (self->child_window_pixbuf);

  g_object_unref (self->shell);
  self->shell = NULL;

  g_rand_free (self->rand);

  g_print ("Cleaned up!\n");

#if 0
  g_print ("Sleeping 5 seconds, make sure we actually quit...\n");
  g_usleep (1000 * 1000 * 5);
#endif
}

static void
_click_cb (XrdShell *shell, XrdClickEvent *event, Example *self)
{
  (void) shell;
  (void) self;
  g_print ("button %d %s at %f, %f\n", event->button,
           event->state ? "pressed" : "released", (double) event->position->x,
           (double) event->position->y);
}

static void
_move_cursor_cb (XrdShell *shell, XrdMoveCursorEvent *event, Example *self)
{
  (void) shell;
  (void) self;
  (void) event;
  /*
  g_print ("move: %f, %f\n",
           event->position->x, event->position->y);
   */
}

static void
_shutdown_cb (GxrContext *context, Example *self)
{
  (void) context;
  g_main_loop_quit (self->loop);
}

static GSList *
_get_windows_by_saturation (Example *self, gboolean is_saturated)
{
  GSList *results = NULL;

  for (GSList *l = self->desktop_window_list; l; l = l->next)
    {
      DesktopWindow *window = l->data;
      XrdWindow     *xrd_window = xrd_shell_lookup_window (self->shell, window);
      /* Ignore child window */
      if (window->is_desaturated != is_saturated
          && !xrd_window_has_parent (xrd_window))
        results = g_slist_append (results, window);
    }

  return results;
}

static void *
_reupload_thread (gpointer data)
{
  Example *self = (Example *) data;

  G3kContext   *g3k = xrd_shell_get_g3k (self->shell);
  VkImageLayout layout = g3k_context_get_upload_layout (g3k);

  while (!g_atomic_int_get (&self->shutdown_threads))
    {
      g_usleep (10000);

      if (!self->rendering)
        continue;

      g_mutex_lock (&self->window_mutex);
      GSList *todo_windows = _get_windows_by_saturation (self,
                                                         !self->saturating);

      guint len = g_slist_length (todo_windows);

      if (len == 0)
        {
          self->saturating = !self->saturating;
          todo_windows = _get_windows_by_saturation (self, !self->saturating);
          len = g_slist_length (todo_windows);
          g_assert (len > 0);
        }

      gint32         id = g_rand_int_range (self->rand, 0, (gint32) len);
      GSList        *nth = g_slist_nth (todo_windows, (guint) id);
      DesktopWindow *desktop_window = nth->data;

      g_slist_free (todo_windows);

      XrdWindow *xrd_window = xrd_shell_lookup_window (self->shell,
                                                       desktop_window);

      if (self->extra_window)
        xrd_window = self->extra_window;

      if (!xrd_window)
        {
          g_printerr ("Error processing frame, window is NULL\n");
          g_mutex_unlock (&self->window_mutex);
          continue;
        }

      GdkPixbuf *pixbuf = NULL;
      if (self->saturating)
        pixbuf = self->window_pixbuf;
      else
        pixbuf = self->bw_window_pixbuf;

      GulkanTexture *tex = g3k_plane_get_texture (G3K_PLANE (xrd_window));
      if (!tex)
        {
          g_printerr ("Error processing frame, window has no texture\n");
          g_mutex_unlock (&self->window_mutex);
          continue;
        }

      gulkan_texture_upload_pixbuf (tex, pixbuf, layout);
      desktop_window->is_desaturated = !self->saturating;

      g_mutex_unlock (&self->window_mutex);
    }

  return NULL;
}

static gboolean
_init_threads (Example *self)
{
  GError *error = NULL;
  self->upload_thread = g_thread_try_new ("upload",
                                          (GThreadFunc) _reupload_thread, self,
                                          &error);
  if (error != NULL)
    {
      g_printerr ("Unable to start render thread: %s\n", error->message);
      g_error_free (error);
      return FALSE;
    }

  return TRUE;
}

static void
_state_change_cb (GxrContext *gxr, GxrStateChangeEvent *event, gpointer _self)
{
  (void) gxr;

  Example *self = (Example *) _self;

  switch (event->state_change)
    {
      case GXR_STATE_SHUTDOWN:
        break;
      case GXR_STATE_FRAMECYCLE_START:
        self->framecycle = TRUE;
        g_debug ("shell: start frame cycle");
        break;
      case GXR_STATE_FRAMECYCLE_STOP:
        self->framecycle = FALSE;
        g_debug ("shell: stop frame cycle");
        break;
      case GXR_STATE_RENDERING_START:
        self->rendering = TRUE;
        g_debug ("shell: start rendering");
        break;
      case GXR_STATE_RENDERING_STOP:
        self->rendering = FALSE;
        g_debug ("shell: stop rendering");
        break;
    }
}

static gboolean
_init_shell (Example *self, XrdShell *shell)
{
  if (!shell)
    {
      g_printerr ("XrdShell did not initialize correctly.\n");
      return FALSE;
    }

  self->click_source = g_signal_connect (shell, "click-event",
                                         (GCallback) _click_cb, self);
  self->move_source = g_signal_connect (shell, "move-cursor-event",
                                        (GCallback) _move_cursor_cb, self);

  G3kContext *g3k = xrd_shell_get_g3k (self->shell);
  g_signal_connect (g3k, "shutdown", G_CALLBACK (_shutdown_cb), self);

  if (!_init_cursor (self))
    return FALSE;

  _init_buttons (self);

  g_signal_connect (g3k_context_get_gxr (g3k), "state-change-event",
                    (GCallback) _state_change_cb, self);

  return TRUE;
}

static gboolean
_init_example (Example *self, XrdShell *shell)
{
  self->shell = shell;
  self->shutdown = FALSE;

  if (!_init_shell (self, shell))
    return FALSE;

  if (!_init_windows (self))
    return FALSE;

  g_unix_signal_add (SIGINT, _sigint_cb, self);

  _desktop_window_manager_update_textures (self);

  _init_threads (self);

  g3k_context_start_renderer (xrd_shell_get_g3k (self->shell));

  return TRUE;
}

static int
_run (gboolean *restart)
{
  Example self = {
    .loop = g_main_loop_new (NULL, FALSE),
    .window_pixbuf = load_gdk_pixbuf ("/res/hawk.jpg"),
    .bw_window_pixbuf = load_gdk_pixbuf ("/res/hawk.jpg"),
    .child_window_pixbuf = load_gdk_pixbuf ("/res/cat.jpg"),
    .desktop_window_list = NULL,
    .restart = FALSE,
    .rand = g_rand_new (),
    .shutdown_threads = 0,
  };
  g_mutex_init (&self.window_mutex);

  gdk_pixbuf_saturate_and_pixelate (self.bw_window_pixbuf,
                                    self.bw_window_pixbuf, 0.0, FALSE);

  XrdShell *shell = xrd_shell_new ();
  if (!_init_example (&self, shell))
    return EXIT_FAILURE;

  /* start glib main loop */
  g_main_loop_run (self.loop);

  *restart = self.restart;

  /* don't clean up when quitting during switching */
  if (self.shell != NULL)
    _cleanup (&self);

  g_main_loop_unref (self.loop);

  return EXIT_SUCCESS;
}

int
main ()
{
  while (TRUE)
    {
      gboolean restart = FALSE;
      int      res = _run (&restart);
      if (!restart)
        return res;

      g_print ("Restarting\n");
    }
}
