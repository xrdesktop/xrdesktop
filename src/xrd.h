/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef XRD_H
#define XRD_H

#define XRD_INSIDE

#include "xrd-input-synth.h"
#include "xrd-shake-compensator.h"
#include "xrd-shell.h"
#include "xrd-version.h"
#include "xrd-window.h"

#undef XRD_INSIDE

#endif /* XRD_H */
