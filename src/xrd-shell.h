/*
 * xrdesktop
 * Copyright 2019 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * Author: Manas Chaudhary <manaschaudhary2000@gmail.com>
 * SPDX-License-Identifier: MIT
 */

#ifndef XRD_SHELL_H_
#define XRD_SHELL_H_

#if !defined(XRD_INSIDE) && !defined(XRD_COMPILATION)
#error "Only <xrd.h> can be included directly."
#endif

#include <g3k.h>

#include "xrd-input-synth.h"
#include "xrd-window.h"

G_BEGIN_DECLS

/*
 * Since the pulse animation surrounds the tip and would
 * exceed the canvas size, we need to scale it to fit the pulse.
 */
#define XRD_TIP_VIEWPORT_SCALE 3

#define XRD_TYPE_SHELL xrd_shell_get_type ()
G_DECLARE_FINAL_TYPE (XrdShell, xrd_shell, XRD, SHELL, GObject)

XrdShell *
xrd_shell_new_from_vulkan_extensions (GSList *instance_ext_list,
                                      GSList *device_ext_list);

XrdShell *
xrd_shell_new (void);

XrdShell *
xrd_shell_new_from_g3k (G3kContext *g3k);

void
xrd_shell_add_window (XrdShell  *self,
                      XrdWindow *window,
                      G3kObject *parent,
                      gboolean   draggable,
                      gpointer   lookup_key);

XrdWindow *
xrd_shell_lookup_window (XrdShell *self, gpointer key);

void
xrd_shell_remove_window (XrdShell *self, XrdWindow *window);

void
xrd_shell_add_button (XrdShell           *self,
                      G3kButton          *button,
                      graphene_point3d_t *position,
                      GCallback           press_callback,
                      gpointer            press_callback_data,
                      G3kObject          *parent);

G3kKeyboard *
xrd_shell_get_keyboard (XrdShell *self);

XrdWindow *
xrd_shell_get_keyboard_window (XrdShell *self);

G3kContext *
xrd_shell_get_g3k (XrdShell *self);

XrdWindow *
xrd_shell_get_synth_hovered (XrdShell *self);

void
xrd_shell_set_pin (XrdShell *self, XrdWindow *win, gboolean pin);

void
xrd_shell_show_pinned_only (XrdShell *self, gboolean pinned_only);

XrdInputSynth *
xrd_shell_get_input_synth (XrdShell *self);

gboolean
xrd_shell_poll_runtime_events (XrdShell *self);

G3kCursor *
xrd_shell_get_desktop_cursor (XrdShell *self);

void
xrd_shell_set_desktop_cursor_position (XrdShell         *self,
                                       XrdWindow        *window,
                                       graphene_point_t *pixels);

G_END_DECLS

#endif /* XRD_SHELL_H_ */
