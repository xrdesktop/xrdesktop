/*
 * xrdesktop
 * Copyright 2020 Collabora Ltd.
 * Author: Christoph Haag <christoph.haag@collabora.com>
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include <g3k.h>
#include <glib.h>
#include <xrd.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#define EGL_EGLEXT_PROTOTYPES
#include <EGL/egl.h>
#include <EGL/eglext.h>

static GdkPixbuf *
_load_gdk_pixbuf (const gchar *name, int w)
{
  GError    *error = NULL;
  GdkPixbuf *pixbuf_rgb = gdk_pixbuf_new_from_resource (name, &error);

  if (error != NULL)
    {
      g_printerr ("Unable to read file: %s\n", error->message);
      g_error_free (error);
      return NULL;
    }

  GdkPixbuf *pixbuf = gdk_pixbuf_add_alpha (pixbuf_rgb, FALSE, 0, 0, 0);
  g_object_unref (pixbuf_rgb);

  int   width = gdk_pixbuf_get_width (pixbuf);
  int   height = gdk_pixbuf_get_height (pixbuf);
  float aspect = (float) width / (float) height;

  int        h = (int) ((float) w / aspect);
  GdkPixbuf *pixbuf_scaled = gdk_pixbuf_scale_simple (pixbuf, w, h,
                                                      GDK_INTERP_NEAREST);
  g_object_unref (pixbuf);

  return pixbuf_scaled;
}

static GulkanTexture *
_make_texture (GulkanContext *gc,
               VkImageLayout  layout,
               const gchar   *resource,
               int            w)
{
  GdkPixbuf *pixbuf = _load_gdk_pixbuf (resource, w);
  if (pixbuf == NULL)
    {
      g_printerr ("Could not load image.\n");
      return FALSE;
    }

  VkExtent2D extent = {(uint32_t) gdk_pixbuf_get_width (pixbuf),
                       (uint32_t) gdk_pixbuf_get_height (pixbuf)};

  guchar *rgb = gdk_pixbuf_get_pixels (pixbuf);

  gsize          size;
  int            fd;
  GulkanTexture *texture
    = gulkan_texture_new_export_fd (gc, extent, VK_FORMAT_R8G8B8A8_UNORM,
                                    layout, &size, &fd);
  g_print ("Mem size: %lu\n", size);
  if (texture == NULL)
    {
      g_printerr ("Unable to initialize vulkan dmabuf texture.\n");
      return NULL;
    }

  GLint  gl_dedicated_mem = GL_TRUE;
  GLuint gl_mem_object;
  glCreateMemoryObjectsEXT (1, &gl_mem_object);
  glMemoryObjectParameterivEXT (gl_mem_object, GL_DEDICATED_MEMORY_OBJECT_EXT,
                                &gl_dedicated_mem);

  glMemoryObjectParameterivEXT (gl_mem_object, GL_DEDICATED_MEMORY_OBJECT_EXT,
                                &gl_dedicated_mem);
  glGetMemoryObjectParameterivEXT (gl_mem_object,
                                   GL_DEDICATED_MEMORY_OBJECT_EXT,
                                   &gl_dedicated_mem);

  glImportMemoryFdEXT (gl_mem_object, size, GL_HANDLE_TYPE_OPAQUE_FD_EXT, fd);

  GLuint gl_texture;

  glGenTextures (1, &gl_texture);
  glActiveTexture (GL_TEXTURE0);
  glBindTexture (GL_TEXTURE_2D, gl_texture);

  /* this does not seem to be necessary to get everything in linear memory but
   * for now we leave it here */
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_TILING_EXT, GL_OPTIMAL_TILING_EXT);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  glTexStorageMem2DEXT (GL_TEXTURE_2D, 1, GL_RGBA8, extent.width, extent.height,
                        gl_mem_object, 0);

  /* don't need to keep this around */
  glDeleteMemoryObjectsEXT (1, &gl_mem_object);

  glTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, (GLsizei) extent.width,
                   (GLsizei) extent.height, GL_RGBA, GL_UNSIGNED_BYTE,
                   (GLvoid *) rgb);

  glFinish ();

  g_object_unref (pixbuf);

  return texture;
}

static gboolean
_similar (float a, float b)
{
  return fabs ((double) a - (double) b) < 0.01;
}

static VkExtent2D extent;
static float      expected_mouse_x = 0;
static float      expected_mouse_y = 0;
static gboolean   success = FALSE;
static void
_move_cursor_cb (XrdShell *shell, XrdMoveCursorEvent *event, gpointer *_)
{
  (void) shell;
  (void) _;
  (void) event;

  float x = event->position->x;
  float y = (float) extent.height - event->position->y;

  g_print ("move: %f, %f\n", (double) x, (double) y);
  if (_similar (expected_mouse_x, x) || !_similar (expected_mouse_y, y))
    success = TRUE;
  else
    {
      g_print ("Error: mouse move to unexpected location!\n");
      g_print ("IS    : %f, %f\n", (double) event->position->x, (double) y);
      g_print ("SHOULD: %f %f\n", (double) expected_mouse_x,
               (double) expected_mouse_y);
      success = FALSE;
    }
}

static void
_cleanup (XrdShell *shell)
{
  g_object_unref (shell);
}

static gboolean
_test_move (float     left,
            float     bottom,
            float     ppm,
            float     dist,
            XrdShell *shell,
            int       x,
            int       y)
{
  g_print ("Test %d %d\n", x, y);
  graphene_vec3_t eye;
  graphene_vec3_init (&eye, 0, 0, 0);

  graphene_vec3_t to;
  graphene_vec3_init (&to, left + (float) x / ppm, bottom + (float) y / ppm,
                      dist);

  graphene_matrix_t pose;
  graphene_matrix_init_look_at (&pose, &eye, &to, graphene_vec3_y_axis ());

  expected_mouse_x = (float) x;
  expected_mouse_y = (float) y;

  G3kContext    *g3k = xrd_shell_get_g3k (shell);
  G3kController *controller = g3k_context_init_dummy_controller (g3k);
  GxrController *gxr_controller = g3k_controller_get_controller (controller);

  GxrPoseEvent event = {
    .active = TRUE,
    .controller = 0,
    .device_connected = TRUE,
    .valid = TRUE,
    .pose = pose,
  };
  gxr_controller_update_pointer_pose (gxr_controller, &event);

  G3kObjectManager *manager = g3k_context_get_manager (g3k);
  g3k_object_manager_update_controller (manager, controller);

  g_object_unref (controller);

  return success;
}

static int
_test_window_external_memory ()
{
  XrdShell *shell = xrd_shell_new ();
  if (shell == NULL)
    {
      g_printerr ("Could not init scene shell.\n");
      g_object_unref (shell);
      return EXIT_FAILURE;
    }

  g_signal_connect (shell, "move-cursor-event", (GCallback) _move_cursor_cb,
                    shell);

  G3kContext    *g3k = xrd_shell_get_g3k (shell);
  GulkanContext *gulkan = g3k_context_get_gulkan (g3k);
  VkImageLayout  layout = g3k_context_get_upload_layout (g3k);
  GulkanTexture *texture = _make_texture (gulkan, layout, "/res/cat.jpg", 800);
  if (!texture)
    {
      g_printerr ("Could not create texture.\n");
      return EXIT_FAILURE;
    }

  extent = gulkan_texture_get_extent (texture);
  float aspect = (float) extent.width / (float) extent.height;

  float window_width_meter = 2.0;
  float window_height_meter = window_width_meter / aspect;
  float ppm = (float) extent.width / window_width_meter;

  graphene_size_t size_meters = {window_width_meter, window_height_meter};

  XrdWindow *window = xrd_window_new (xrd_shell_get_g3k (shell), "win.", NULL,
                                      extent, &size_meters);

  float left = 0.5f;
  float right = left + window_width_meter;
  float bottom = 1.5f;
  float top = bottom + window_height_meter;

  float dist = -3.f;

  graphene_point3d_t point = {
    .x = left + (right - left) / 2.f,
    .y = bottom + (top - bottom) / 2.f,
    .z = dist,
  };
  graphene_matrix_t transform;
  graphene_matrix_init_translate (&transform, &point);
  g3k_object_set_matrix (G3K_OBJECT (window), &transform);

  g3k_plane_set_texture (G3K_PLANE (window), texture);

  xrd_shell_add_window (XRD_SHELL (shell), XRD_WINDOW (window), NULL, TRUE,
                        NULL);

  GulkanDevice *device = gulkan_context_get_device (gulkan);
  gulkan_device_wait_idle (device);

  g3k_context_render (g3k);
  gulkan_device_wait_idle (device);

  g3k_context_render (g3k);
  gulkan_device_wait_idle (device);

  const int ms_delay = 150;

  if (!_test_move (left, bottom, ppm, dist, XRD_SHELL (shell), 100, 100))
    {
      g_printerr ("Move test 1/3 failed.\n");
      _cleanup (XRD_SHELL (shell));
      return EXIT_FAILURE;
    }

  for (int i = 0; i < ms_delay; i += 10)
    {
      g3k_context_render (g3k);
      gulkan_device_wait_idle (device);
      g_usleep (ms_delay / 10);
    }

  if (!_test_move (left, bottom, ppm, dist, XRD_SHELL (shell), 10, 300))
    {
      g_printerr ("Move test 2/3 failed.\n");
      _cleanup (XRD_SHELL (shell));
      return EXIT_FAILURE;
    }

  for (int i = 0; i < ms_delay; i += 10)
    {
      g3k_context_render (g3k);
      gulkan_device_wait_idle (device);
      g_usleep (ms_delay / 10);
    }

  if (!_test_move (left, bottom, ppm, dist, XRD_SHELL (shell), 400, 10))
    {
      g_printerr ("Move test 3/3 failed.\n");
      _cleanup (XRD_SHELL (shell));
      return EXIT_FAILURE;
    }

  for (int i = 0; i < ms_delay; i += 10)
    {
      g3k_context_render (g3k);
      gulkan_device_wait_idle (device);
      g_usleep (ms_delay / 10);
    }

  _cleanup (XRD_SHELL (shell));

  return EXIT_SUCCESS;
}

int
main ()
{
  if (!glfwInit ())
    {
      g_printerr ("Unable to initialize GLFW3\n");
      return EXIT_FAILURE;
    }
  glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 2);
  glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  glfwWindowHint (GLFW_VISIBLE, GLFW_FALSE);

  GLFWwindow *window = glfwCreateWindow (640, 480, "GLFW OpenGL & Vulkan", NULL,
                                         NULL);

  if (!window)
    {
      g_printerr ("Unable to create window with GLFW3\n");
      glfwTerminate ();
      return EXIT_FAILURE;
    }
  glfwMakeContextCurrent (window);

  glewInit ();

  const GLubyte *renderer = glGetString (GL_RENDERER);
  const GLubyte *version = glGetString (GL_VERSION);
  g_print ("OpengL Renderer: %s\n", renderer);
  g_print ("OpenGL Version : %s\n", version);

  return _test_window_external_memory ();
}
